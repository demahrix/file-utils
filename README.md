
# Quelques fonctions utils pour gérer les fichiers sur nodeJs

## Uploader un fichier

```javascript
const FileUtils = require("file_utils");
const url = await FileUtils.upload({
    file: request.body.myImage,
    directory: "public/images/profile_images",
    url: "http://127.0.0.1:5009",
    name: "mon_image",
    type: FileUtils.type.IMAGE,
    fileTypeError: new ApiError(415, "type d'image non supporté")
});

if (url != null) {
    // url: http://127.0.0.1:5009/public/images/profile_images/mon_image.extension
    console.log("image enregistré!");
}
```
