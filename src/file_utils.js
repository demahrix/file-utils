const path = require("path");
const fs = require("fs/promises");

// FIXME Terminer la suppression des fichers
// Implementer un methode pour remplacer un fichier

class FileUtils {

    static type = {
        IMAGE: ['jpeg', 'jpg', 'png', 'webp'], // add svg, gif, tiff
        VIDEO: ['mp4', 'mkv', 'avi', 'ts', 'm4v', 'mov', 'flv', 'wmv', 'mpeg', 'mpg', 'ogv', 'webm'],
        DOCUMENT: ['pdf', 'ppt', 'pptx', 'doc', 'docx', 'xls', 'xlsx'],
        AUDIO: ['mp3', 'aac', 'wav', 'ogg'],
        ANY: null
    };

    // FIXME: gerer la taille du fichier
    /**
     * `file` le fichier contenu dans le corps de la requete \
     * `directory` le dossier parent où l'image doit etre enregistrer \
     * `name` le nom sous laquel on souhaite enregistré l'image sans extension\
     * `url` url du site (ex: `http://abc.com`) \
     * `types` Les extensions autorisees  \
     * `fileTypeError` exception à lever quand le type de fichier est incorrect
     * @param {{file: unknown, directory: string, name: string, url: string, types: string[]?, fileTypeError: Error }} param0 
     * @returns {Promise<string>} Promesse contenant l'url de l'image ou null
     */
    static async upload({ file, directory, name, url, types, fileTypeError }) {
        try {
            const index = file.filename.lastIndexOf('.');
            const extension = index != -1 ? file.filename.substr(index + 1) : null;

            if (types && !types.includes(extension))
                throw fileTypeError;

            const p = path.join(directory, name + (extension ? '.' + extension : ''));
            await fs.writeFile(p, await file.toBuffer());
            return url + '/' + path.relative(process.cwd(), p).split('\\').join('/');
        } catch (err) {
            throw err;
        }
    }

    /**
     * 
     * @param {{files: unknown[], directory: string, names: string[], url: string, types: string[]?, fileTypeError: Error }} param0 
     */
    static async uploads({ files, directory, names, url, types, fileTypeError }) {
        const len = files.length;
        /** @type{List<Promise<String>} */
        const promises = new Array(len);
        for (let i=0; i<len; ++i)
            promises[i] = FileUtils.upload({ file: files[i], directory, name: names[i], url, types, fileTypeError });

        const results = [];

        try {
            for await (const p of promises)
                results.push(p);
            return results;
        } catch(err) {
            // rollback
            // Supprimer les fichier déjà rengistré
            for (const url of results) {}

            throw err;
        }
    }

    /**
     * 
     * @param {String} url 
     * @returns {Promise<string} Le chemin absolue de fichier sur le disque
     */
     static async delete(url) {
        const pathname = path.join(process.cwd(), new URL(url).pathname);
        try {
            await fs.access(pathname, fs.constants.F_OK);
            await fs.unlink(pathname);
            return pathname;
        } catch (error) {
            throw error;
        }
    }

}

module.exports = FileUtils;